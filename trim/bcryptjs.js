const bcrypt = require('bcryptjs');

//加密
exports.encryption = function(encrypt){
    let salt = bcrypt.genSaltSync(10);
    let hash = bcrypt.hashSync(encrypt, salt);
    return hash;
}

//解密 
exports.decryption = function(decrypt,hash) {
    let data = bcrypt.compareSync(decrypt, hash);
    return data;
}