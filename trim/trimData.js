const users = require('../dbCRDU/users');
const friendList = require('../dbCRDU/friendList');
const messages = require('../dbCRDU/messages');



const jwt = require('./jwt');

//处理登录的邮箱密码效验
exports.login = function (req,res){
    const {email,psw} = req.body;
    users.loginTest(email,psw,res);
}

//整理邮箱
exports.signInEmail = function (req,res){
    const {email} = req.body;
    users.signInEmailTest(email,res);
}

//整理注册信息
exports.signIn = function (req,res){
    const {username,email,password} = req.body;
    users.signInSave(username,email,password,res);
}

//整理token,并判断是否过期，并直接返回给前端响应
exports.token = function (req,res){
    const {token} = req.body;
    jwt.decodejwt(token,function(err,decode){
        if(err){
           res.send({status: 400});
        } else {
            res.send({status: 200});
        }
    });
    
}

//搜索用户
exports.searchUser = function(req,res){
    const {searchUser,meId} = req.body;
    users.searchUser(searchUser,meId,res);
}
//搜索到的用户和自己是什么关系的数据
exports.usersStatus = function(req,res){
    const {meId} = req.body;
    friendList.searchUser(meId,res);
}


//添加好友请求
exports.addFriend = function(req,res){
    const {userId,meId,remark,friendStatus,time} = req.body;
    friendList.addFriend(userId,meId,remark,friendStatus,time,res);
}

//修改用户名
exports.alterName =function(req,res){
    const {username,meId} = req.body;
    users.alterUsername(username,meId,res);
}

//修改签名
exports.alterSign = function(req,res){
    const {sign,meId} = req.body;
    users.alterSign(sign,meId,res);
}

//新的朋友
exports.newFriend = function(req,res){
    const {meId} = req.body;
    friendList.newFriend(meId,res);
}

//请求用户的个人信息
exports.reqFriendsInfo = function(req,res){
    const {usersId} = req.body;
    users.reqFriendsInfo(usersId,res);
}

//拒绝好友请求
exports.refuseFriend = function(req,res){
    const {refuseId,status} = req.body;
    friendList.refuseFriendReq(refuseId,status,res);
}

//同意好友请求
exports.agreeFriendReq = function(req,res){
    const {agreeId,status} = req.body;
    friendList.agreeFriendReq(agreeId,status,res);
}

//判断是不是好友
exports.friendVerify = function(req,res) {
    const {meId} = req.body;
    friendList.friendVerify(meId,res);
}

//好友列表
exports.addressList = function(req,res) {
  const {meId,usersIdAry} = req.body;
  users.addressList(meId,usersIdAry,res);
}

//切换头像
exports.meImg = function(req,res) {
    const {meId,meImg} = req.body;
    users.meImg(meId,meImg,res);
}

// //一对一消息
// exports.message = function(req,res) {
//     const {meId,userId} = req.body;
//     messages.buildmsg(meId,userId,res);
// }

//一对一文字消息存储
exports.saveMessage = function(data,res){
    const {id,userId,img,message,type,tip,state} = data;
    messages.saveMessage(id,userId,img,message,type,tip,state,res);
}

//消息检测有没有
exports.examine = function(req,res) {
    const {meId,userId} = req.body;
    messages.examine(meId,userId,res);
}