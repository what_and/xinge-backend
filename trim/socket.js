const trimData = require('../trim/trimData');

module.exports = function(io) {
    io.on('connection',  (socket)=>{
        // //接收
        socket.on('msg',(data) => {
            //发送的消息是文字
            if(data.type === 0){
                io.emit('receiveMsgs',data);
                trimData.saveMessage(data);
            } else if(data.type === 1){
                io.emit('receiveMsgs',data);
            }
        })

        // //用户离开
        // socket.on('disconnecting',() => {
        //     console.log(socket.id + '离开')
        // })
    })
}

