
const trimData = require('../trim/trimData');
const emailServer = require('../controllers/sendEmail');//发送邮箱

module.exports = function(app){
    //验证用户名和密码是否存在，并且正确。
    app.post('/login', (req, res) => {
        trimData.login(req,res)
    });

    //验证邮箱是否已被注册。
    app.post('/signInEmail',(req,res) => {
        trimData.signInEmail(req,res)
    });

    //注册用户
    app.post('/signIn',(req,res) => {
        // emailServer.sendEmail(res)
        trimData.signIn(req,res);
    });

    //验证token
    app.post('/token',(req,res) => {
        trimData.token(req,res);
    })

    //搜索用户
    app.post('/searchUser',(req,res) => {
        trimData.searchUser(req,res);
    })

    //判断搜索到的用户和自己是什么关系
    app.post('/friendStatus',(req,res) => {
        trimData.usersStatus(req,res);
    })

    //添加好友请求
    app.post('/addFriend',(req,res) => {
        trimData.addFriend(req,res);
    })

    //修改用户名
    app.post('/alterUsername',(req,res) => {
        trimData.alterName(req,res);
    })

    //修改个性签名
    app.post('/alterSign',(req,res) => {
        trimData.alterSign(req,res);
    })

    //新的朋友
    app.post('/newFriend',(req,res) => {
        trimData.newFriend(req,res);
    })

    //请求用户的个人信息
    app.post('/reqFriendsInfo',(req,res) => {
        trimData.reqFriendsInfo(req,res);
    })

    //拒绝好友请求
    app.post('/refuseFriend',(req,res) => {
        trimData.refuseFriend(req,res);
    })

    //同意好友请求
    app.post('/agreeFriendReq',(req,res) => {
        trimData.agreeFriendReq(req,res);
    })

    //是否是好友验证，用来获得是好友的用户id
    app.post('/friendVerify',(req,res) => {
        trimData.friendVerify(req,res);
    })

    //好友列表
    app.post('/addressList',(req,res) => {
        trimData.addressList(req,res);
    })

    //换头像
    app.post('/meImg',(req,res) => {
        trimData.meImg(req,res)
    })

    // //一对一消息
    // app.post('/message',(req,res) => {
    //     trimData.message(req,res);
    // })

    //检查消息是不是空的
    app.post('/examine',(req,res) => {
        trimData.examine(req,res);
    })

    
}