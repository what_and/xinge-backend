//附件上传
let multer = require('multer');
//自动生成目录
let mkdir = require('../trim/mkdir');
// const { urlencoded } = require('body-parser');

//控制文件的存储
var storage = multer.diskStorage({
    destination: function (req, file, cb) {

        let url = req.body.url;
        mkdir.mkdirs('../files/' + url, err => {
            console.log(err);
        })
        //路径
      cb(null, './files/' + url);
    },

    filename: function (req, file, cb) {
        let name = req.body.meId;
        //这个正则的含义是将.前面的所有字符都变为.
        let type = file.originalname.replace(/.+\./,'.');
        //每一个文件名，文件名就是时间戳加上后缀
        console.log(type)
      cb(null, name + type)
    }
  })
   
  var upload = multer({ storage: storage })

module.exports = function(app) {
    //上传头像
    app.post('/photos/upload', upload.array('file', 12), function (req, res, next) {
        //获取图片名字

        let data = req.files[0].filename;
        res.send(data);
    })
    app.post('/photos/sendimg', upload.array('file', 12), function (req, res, next) {
      //获取图片名字

      let data = req.files[0].filename;
      res.send(data);
  })
}