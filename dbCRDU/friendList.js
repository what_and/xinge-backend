const dbmodel = require('../model/dbmodel');
let friendListModel = dbmodel.model('friendList');  


//与用户的状态判断
exports.searchUser = function(meId,res){
    friendListModel.find({$or:[{meId:meId},{userId:meId}]},{meId:1,userId:1,friendStatus:1},function(err,data){
        if(err) {
            res.send({status:500})
        } else {
          res.send({status: 200,data})    
        }
    })
}

//好友请求
exports.addFriend = function(userId,meId,remark,friendStatus,time,res){
  let addFriend = new friendListModel({
    userId,meId,remark,friendStatus,time
  })

  addFriend.save(function(err,data){
    if(err){
        res.send({status:500})
    } else {
        res.send({states: 200});
    }
  })
}

//新的朋友
exports.newFriend = function(meId,res) {
  friendListModel.find({userId:meId,friendStatus:0},function(err,data){
    if(err) {
      res.send({status: 500});
    } else {
      res.send({status: 200,data});
    }
  })
}

//拒绝好友请求,将当前这条数据删除
exports.refuseFriendReq = function(refuseId,status,res) {
  friendListModel.deleteOne({meId:refuseId,friendStatus:status},function(err,data) {
    if(err) {
      res.send({status: 500});
    } else {
      res.send({satus: 200});
    }
  });
}

//同意好友请求
exports.agreeFriendReq = function(agreeId,status,res) {
  friendListModel.update({meId:agreeId},{$set:{friendStatus:2}},function(err,data) {
    if(err) {
      res.send({status: 500});
    } else {
      res.send({status: 200});
    }
  });
}

//好友验证，判断是不是好友
exports.friendVerify = function(meId,res) {

  friendListModel.find({$or: [ {meId},{userId:meId}],friendStatus:2},function(err,data) {
    if(err) {
      res.send({status: 500});
    } else {
      res.send({status: 200,data});
    }
  })
}