const dbmodel = require('../model/dbmodel');
let userModel = dbmodel.model('User');  
let jwt = require('../trim/jwt');

// const bcryptjs = require('../trim/bcryptjs');//数据加密

//登录查询账号和密码是否存在
exports.loginTest = function(email,password,res){
     userModel.findOne({email,password},function(err,data){
         if(err){
             res.send({status: 500})
         }else if(!data){
             //如果不存在
            res.send({status: 400})

         } else {//用户名和密码正确
            //生成token
            let token = jwt.generatejwt(data._id);
            res.send({status: 200,data,token});
         }
     });

}

//注册时检测邮箱是否已被注册
exports.signInEmailTest = function(email,res){
    userModel.findOne({email},function(err,data){
        if(err){
            res.send({status: 500})
        } else if(data){
            //已被注册
            res.send({status: 400})
        } else {
            res.send({status: 200})
        }
    })
}

//保存用户注册的信息
exports.signInSave = function(username,email,password,res){
    //加密邮箱
    // let hashEmail = bcryptjs.encryption(email);

    //加密密码
    // let hashPassword = bcryptjs.encryption(password);

    let signIn = new userModel({
        username:username,
        email: email,
        password: password,
        signTime: new Date()
    });
    signIn.save(function(err,data){
        if(err){
            res.send({status: 500})
        } else {
            console.log(data)
            res.send({status: 200})
        }
    })
    
}

//搜索用户
exports.searchUser = function(searchUser,meId,res){
    let users = new RegExp(searchUser,'i');
    userModel.find({
        $or : [ //多条件，数组
            {username : {$regex : users}},
            {email : {$regex : users}}
        ],
        _id:{$ne:meId}
    },{signature: 0,password:0,sex: 0,birthDay: 0},function(err,data){
        if(err){
            res.send({status: 500})
        }else{
            //整理数据库中的数据
            res.send({status:200,data})
        }
    })
}


//修改用户名
exports.alterUsername = function(username,meId,res) {
    userModel.findOneAndUpdate({_id:meId},{$set:{username}},{new:true},function(err,data){
        if(err){
            res.send({status: 500});
        } else {
            res.send({status:200,data})
        }
    })
}


//修改个性签名
exports.alterSign = function(signature,meId,res) {
    userModel.findOneAndUpdate({_id:meId},{$set:{signature}},{new:true},function(err,data){
        if(err) {
            res.send({status: 500});
        } else {
            res.send({status: 200,data});
        }
    })
}

//请求用户的个人信息
exports.reqFriendsInfo = function(usersId,res) {
    let usersInfo = [];
    let index = 0;
        usersId.forEach(id => {
            userModel.find({_id:id},function(err,data){
                if(err) {
                    res.send({status: 500});
                } else {
                    usersInfo.push(data[0]);
                    index++;
                    if(index === usersId.length) {
                        res.send({status:200,usersInfo});
                    }
                }
            })
        })
}

//好友列表
exports.addressList = function(meId,usersIdAry,res) {
    let friendName = []
    let index = 0;
    usersIdAry.forEach(item => {
        if(item !== meId){
            userModel.find({_id:item, _id:{$ne:meId}},{username:1,imgUrl:1},function(err,docs) {
                if(err) {
                    res.send({status: 500});
                } else {
                    index++;
                    friendName.push(docs);
                    if(index == usersIdAry.length){
                        res.send({status: 200,friendName})
                    }
                }
            })
        } else {
            index++;
        }
       
    })
}


//修改头像
exports.meImg = function(meId,meImg,res) {
    userModel.findOneAndUpdate({_id:meId},{$set:{imgUrl:meImg}},function(err,docs) {
        if(err) {
            res.send({status: 500});
        } else {
            res.send({status: 200})
        }
    })
}