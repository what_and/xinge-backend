const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const port = 3001;

var httpServer = require('http').createServer();
var io = require('socket.io')(httpServer);
httpServer.listen(3002);
require('./trim/socket')(io);


//设置允许跨域访问该服务.
app.all('*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Content-Type', 'application/json;charset=utf-8');
    if(req.method == 'OPTIONS') {
        res.sendStatus(200);
    } else {
        next();
    }
  });


app.use(bodyParser.json());
//引入路由
require('./router/router')(app);
//引入附件上传
require('./router/file')(app);

app.use(express.static(__dirname + '/files'));

//处理404
app.use(function (req, res, next) {
    res.status(404).send("Sorry can't find that!");
    next();
})
//处理服务器错误
app.use(function (err, req, res, next) {
    console.error(err.stack)
    res.status(500).send('Something broke!')
    next();
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`));