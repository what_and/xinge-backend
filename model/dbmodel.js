const mongoose = require('../controllers/db');
const { Schema } = require('../controllers/db');

let userSchema = mongoose.Schema({
    id:Schema.Types.ObjectId,                       //用户id
    imgUrl: {type: String,default: ''},     //头像
    username: String,                               //用户名
    email: String,                                  //邮箱
    sex: {type: String, default: 'mm'},             //性别
    birthDay: {type: String, default: '1997-07-28'},          //生日
    signature: {type: String, default: '你好世界'},  //签名
    password: String,                               //密码
    signTime: Date                                  //注册时间
  }); 

let friendListSchema = mongoose.Schema({
  id:Schema.Types.ObjectId,                   
  userId : {type: Schema.Types.ObjectId,ref: 'users'},
  meId: {type: Schema.Types.ObjectId,ref: 'users'},
  remark: String,
  friendStatus: Number,
  time: Date
  }); 

let MessageSchema = new Schema({
  userId : {type: Schema.Types.ObjectId,ref: 'users'},  //用户Id
  id: {type: Schema.Types.ObjectId,ref: 'users'},     //自己的id
  img: String,                                       //头像
  message: String,
  type: Number,
  tip: Number,
  state: Number,
  time: Date
})

module.exports =  mongoose.model('User', userSchema,'users');               //用户信息集合
module.exports = mongoose.model('friendList',friendListSchema,'friendList');//好友列表集合
module.exports = mongoose.model('Message',MessageSchema,'messages');//一对一消息集合
